<details><summary>Package Counts</summary>

---

Inside of the omnibus-gitlab pipelines, the `Package-and-image-release` stage contain many jobs.  For all jobs that are part of an Operating System Package, count each of them one time, with the exception of CentOS 7, count this one 3 times.

For example, for 14.9.X-ee:

* AmazonLinux-2-arm64-release
* AmazonLinux-2-release
* CentOS-7-release
* CentOS-8-arm64-release
* CentOS-8-release
* Debian-9-release
* Debian-10-arm-release
* Debian-10-release
* Debian-11-arm-release
* Debian-11-release
* OpenSUSE-15.3-arm64-release
* OpenSUSE-15.3-release
* SLES-12.5-release
* SLES-15.2-release
* Ubuntu-18.04-release
* Ubuntu-20.04-arm-release
* Ubuntu-20.04-release

The above would produce 19 total packages.

Points of importance:

* The amount and type of these jobs changes over time, therefore the number of packages created may change with each release
* Reach out to the Distribution team for any questions related to needing to confirm packages
* CE _normally_ has fewer packages

---

</details>

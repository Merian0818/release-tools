# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::PostDeployMigrations::Artifact do
  let(:post_deploy_file) { described_class::FILE_NAME }

  subject(:artifact) { described_class.new }

  after do
    FileUtils.rm_r(post_deploy_file) if File.exist?(post_deploy_file)
  end

  describe '#create' do
    let(:pending_post_migrations) do
      '[
        "20220401044858  Add user id and state index to merge request assignees",
        "20220401045116  Add user id and state index to merge request reviewers",
        "20220401045621  Remove state index on merge request assignees"]'
    end

    context 'with pending post-migrations' do
      it 'creates a file with the artifact of pending post-migrations' do
        post_migrations = [
          '20220401044858_add_user_id_and_state_index_to_merge_request_assignees.rb',
          '20220401045116_add_user_id_and_state_index_to_merge_request_reviewers.rb',
          '20220401045621_remove_state_index_on_merge_request_assignees.rb'
        ]

        ClimateControl.modify(PENDING_MIGRATIONS: pending_post_migrations) do
          artifact.create

          expect(File).to exist(post_deploy_file)
          expect(File.read(post_deploy_file)).to eq(post_migrations.join(','))
        end
      end
    end

    context 'without pending post-migrations' do
      let(:pending_post_migrations) { '[]' }

      it 'does not store the artifact' do
        ClimateControl.modify(PENDING_MIGRATIONS: pending_post_migrations) do
          artifact.create

          expect(File).not_to exist(post_deploy_file)
        end
      end
    end
  end
end

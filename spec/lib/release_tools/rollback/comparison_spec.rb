# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Rollback::Comparison do
  include MetadataHelper

  let!(:fake_client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:new_version) { build(:product_version) }
  let(:current) { new_version }
  let(:target) { new_version }
  let(:minimum_version) { new_version }

  def stub_compare(comparison)
    allow(fake_client).to receive(:compare).and_return(comparison)
  end

  subject(:comparison) do
    described_class.new(current: current, target: target)
  end

  before do
    metadata = build_metadata(gitlab_sha: 'ad89f6752f3', omnibus_sha: 'ef94506739c')

    allow(current)
      .to receive(:metadata)
      .and_return(metadata)

    allow(target)
      .to receive(:metadata)
      .and_return(metadata)
  end

  describe '#execute' do
    it 'compares from the target to the current deployment' do
      expect(fake_client).to receive(:compare).with(
        described_class::PROJECT,
        from: target[ReleaseTools::Project::GitlabEe.metadata_project_name].sha,
        to: current[ReleaseTools::Project::GitlabEe.metadata_project_name].sha
      ).and_return(build(:compare))

      instance = described_class.new(current: current, target: target)
      instance.execute
    end
  end

  describe '#safe' do
    it 'considers a diff with migrations as safe' do
      compare = create(
        :compare,
        diffs: [
          { 'new_path' => 'db/migrate/foo.rb', 'new_file' => true },
          { 'new_path' => 'db/migrate/bar.rb', 'new_file' => false }
        ]
      )
      stub_compare(compare)

      check = described_class
        .new(current: current, target: target)
        .execute

      expect(check).to be_safe
      expect(check.migrations.size).to eq(1)
      expect(check.post_deploy_migrations.size).to eq(0)
    end

    it 'considers a diff with post-deploy migrations as unsafe' do
      compare = create(
        :comparison,
        diffs: [
          { 'new_path' => 'db/post_migrate/foo.rb', 'new_file' => true },
          { 'new_path' => 'db/post_migrate/bar.rb', 'new_file' => false }
        ]
      )
      stub_compare(compare)

      check = described_class
        .new(current: current, target: target)
        .execute

      expect(check).not_to be_safe
      expect(check.migrations.size).to eq(0)
      expect(check.post_deploy_migrations.size).to eq(1)
    end

    it 'considers a compare timeout as unsafe' do
      compare = create(
        :comparison,
        diffs: [
          { 'new_path' => 'README.md', 'new_file' => true },
          { 'new_path' => 'db/post_migrate/bar.rb', 'new_file' => false }
        ],
        compare_timeout: true
      )
      stub_compare(compare)

      check = described_class
        .new(current: new_version, target: new_version)
        .execute

      expect(check).not_to be_safe
      expect(check.migrations.size).to eq(0)
      expect(check.post_deploy_migrations.size).to eq(0)
    end

    it 'considers an empty diff as unsafe' do
      compare = create(:comparison, diffs: [])

      stub_compare(compare)
      check = described_class
        .new(current: current, target: target)
        .execute

      expect(check).not_to be_safe
      expect(check.migrations.size).to eq(0)
      expect(check.post_deploy_migrations.size).to eq(0)
    end

    it 'considers a diff otherwise safe' do
      compare = create(
        :comparison,
        diffs: [
          { 'new_path' => 'README.md', 'new_file' => true },
          { 'new_path' => 'db/migrate/foo.rb', 'new_file' => false }
        ]
      )
      stub_compare(compare)

      check = described_class
        .new(current: current, target: target)
        .execute

      expect(check).to be_safe
    end

    context 'with rollback_check_post_deploy_pipeline feature flag enabled' do
      before do
        enable_feature(:rollback_check_post_deploy_pipeline)

        allow(ReleaseTools::GitlabOpsClient)
          .to receive(:last_successful_deployment)
          .with(ReleaseTools::Project::Release::Metadata, "db/gprd")
          .and_return(double(sha: 'sha1'))

        allow(ReleaseTools::ProductVersion)
          .to receive(:from_metadata_sha)
          .with('sha1')
          .and_return(minimum_version)
      end

      context 'when target version is older than minimum version' do
        let(:current) { ReleaseTools::ProductVersion.new('15.2.202206301720') }
        let(:target) { ReleaseTools::ProductVersion.new('15.2.202206301320') }
        let(:minimum_version) { ReleaseTools::ProductVersion.new('15.2.202206301620') }

        it 'is unsafe' do
          check = described_class
            .new(current: current, target: target)
            .execute

          expect(check).not_to be_safe
        end
      end

      context 'when target version is same as minimum version' do
        let(:current) { ReleaseTools::ProductVersion.new('15.2.202206301720') }
        let(:target) { ReleaseTools::ProductVersion.new('15.2.202206301320') }
        let(:minimum_version) { ReleaseTools::ProductVersion.new('15.2.202206301320') }

        it 'is safe' do
          compare = create(
            :comparison,
            diffs: [{ 'new_path' => 'db/post_migrate/foo.rb', 'new_file' => true }]
          )
          stub_compare(compare)

          check = described_class
            .new(current: current, target: target)
            .execute

          expect(check).to be_safe
        end
      end

      context 'when target version is newer than minimum version' do
        let(:current) { ReleaseTools::ProductVersion.new('15.2.202206301720') }
        let(:target) { ReleaseTools::ProductVersion.new('15.2.202206301620') }
        let(:minimum_version) { ReleaseTools::ProductVersion.new('15.2.202206301320') }

        it 'is safe' do
          check = described_class
            .new(current: current, target: target)
            .execute

          expect(check).to be_safe
        end
      end
    end
  end

  describe '#current_auto_deploy_package' do
    it 'returns auto_deploy package' do
      expect(comparison.current_auto_deploy_package)
        .to eq('42.1.2021110116-ad89f6752f3.ef94506739c')
    end
  end

  describe '#target_auto_deploy_package' do
    it 'returns auto_deploy package' do
      expect(comparison.target_auto_deploy_package)
        .to eq('42.1.2021110116-ad89f6752f3.ef94506739c')
    end
  end
end

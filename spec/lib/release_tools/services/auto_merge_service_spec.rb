# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Services::AutoMergeService do
  let(:merge_request) { double('ReleaseTools::MergeRequest', project: double, url: 'a url') }
  let(:token) { 'a token' }
  let(:commit) { nil }

  subject(:service) { described_class.new(merge_request, token: token, commit: commit) }

  describe '#execute' do
    context 'when Merge Train is disabled' do
      it 'calls #merge_when_pipeline_succeeds' do
        allow(ReleaseTools::GitlabClient).to receive(:merge_trains_enabled?).with(merge_request.project).and_return(false)

        expect(service).to receive(:merge_when_pipeline_succeeds)

        without_dry_run do
          service.execute
        end
      end
    end

    context 'when Merge Train is enabled' do
      let(:commit) { double('commit') }

      it 'calls #add_to_merge_train for the given commit' do
        allow(ReleaseTools::GitlabClient).to receive(:merge_trains_enabled?).with(merge_request.project).and_return(true)

        expect(service).to receive(:add_to_merge_train)

        without_dry_run do
          service.execute
        end
      end
    end
  end

  describe '#add_to_merge_train' do
    let(:commit) { double('commit', id: 42) }

    it 'add to Merge Train with the gitlab_bot_token' do
      sub_service = double('sub-service')
      allow(ReleaseTools::Services::AddToMergeTrainService)
        .to receive(:new)
        .with(merge_request, token: token)
        .and_return(sub_service)

      expect(sub_service).to receive(:execute).with(commit.id)

      service.__send__(:add_to_merge_train)
    end
  end

  describe '#merge_when_pipeline_succeeds' do
    it 'set MWPS with the gitlab_bot_token' do
      sub_service = double('sub-service')
      allow(ReleaseTools::Services::MergeWhenPipelineSucceedsService)
        .to receive(:new)
        .with(merge_request, token: token)
        .and_return(sub_service)

      expect(sub_service).to receive(:execute)

      service.__send__(:merge_when_pipeline_succeeds)
    end
  end
end

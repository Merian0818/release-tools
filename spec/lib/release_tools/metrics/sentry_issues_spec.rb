# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Metrics::SentryIssues do
  include_context 'metric registry'

  subject(:service) { described_class.new }

  describe '#execute' do
    def stub_versions(result)
      query_spy = spy(rails_version_by_role: result)

      stub_const('ReleaseTools::Prometheus::Query', query_spy)
    end

    it 'performs one request per unique SHA' do
      stub_versions({ 'gstg-cny' => 'aaa', 'gstg' => 'aaa', 'gprd-cny' => 'aaa', 'gprd' => 'aaa' })
      request = stub_request(:get, Regexp.new(described_class::SENTRY_ENDPOINT))

      service.execute

      expect(request).to have_been_requested.once
    end

    it 'sets the number of Sentry issues per version' do
      gauge = stub_gauge(registry)

      stub_versions({ 'gstg-cny' => 'aaa' })
      request = stub_request(:get, "#{described_class::SENTRY_ENDPOINT}?query=first-release:aaa%20is:unresolved")
        .to_return(status: 200, headers: { 'X-Hits' => '30' })

      service.execute

      expect(request).to have_been_requested.once
      expect(gauge).to have_received(:set)
        .with(30, labels: { role: 'gstg-cny' })
      expect(pushgateway).to have_received(:replace).with(registry)
    end

    it 'sets the number to 0 if the request fails' do
      gauge = stub_gauge(registry)

      stub_versions({ 'gstg-cny' => 'aaa' })
      request = stub_request(:get, Regexp.new(described_class::SENTRY_ENDPOINT))
        .to_return(status: 500)

      service.execute

      expect(request).to have_been_requested.once
      expect(gauge).to have_received(:set)
        .with(0, labels: { role: 'gstg-cny' })
      expect(pushgateway).to have_received(:replace).with(registry)
    end

    it 'skips a role with no version information' do
      gauge = stub_gauge(registry)

      stub_versions({ 'gstg-ref' => '' })
      request = stub_request(:get, Regexp.new(described_class::SENTRY_ENDPOINT))

      service.execute

      expect(request).not_to have_been_requested
      expect(gauge).not_to have_received(:set)
      expect(pushgateway).to have_received(:replace).with(registry)
    end
  end
end

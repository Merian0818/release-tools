# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Runtime do
  subject(:runtime) { described_class }

  let(:ci_server_url_env) { "https://example.com" }

  valid_environments = {
    ops: 'https://ops.gitlab.net',
    production: 'https://gitlab.com',
    dev: 'https://dev.gitlab.org'
  }.freeze

  around do |tests|
    ClimateControl.modify(CI_SERVER_URL: ci_server_url_env, &tests)
  end

  describe ".ci_server_url" do
    subject { runtime.ci_server_url }

    it 'returns ENV["CI_SERVER_URL"]' do
      expect(subject).to eq(ci_server_url_env)
    end

    context "when the env is not set" do
      let(:ci_server_url_env) { nil }

      it { is_expected.to be_nil }
    end
  end

  describe ".current_environment" do
    subject { runtime.current_environment }

    context 'when the env is not set' do
      let(:ci_server_url_env) { nil }

      it { is_expected.to be_nil }
    end

    context 'when the env is unknown' do
      let(:ci_server_url_env) { 'https://gitlab.example.com' }

      it { is_expected.to be_nil }
    end

    valid_environments.each do |ret, env|
      context "when CI_SERVER_URL is #{env}" do
        let(:ci_server_url_env) { env }

        it { is_expected.to eq(ret) }
      end
    end
  end

  describe ".valid_environment?" do
    context 'when the env is not set' do
      let(:ci_server_url_env) { nil }

      it { is_expected.not_to be_valid_environment }
    end

    context 'when the env is unknown' do
      let(:ci_server_url_env) { 'https://gitlab.example.com' }

      it { is_expected.not_to be_valid_environment }
    end

    valid_environments.each do |_, env|
      context "when CI_SERVER_URL is #{env}" do
        let(:ci_server_url_env) { env }

        it { is_expected.to be_valid_environment }
      end
    end
  end
end

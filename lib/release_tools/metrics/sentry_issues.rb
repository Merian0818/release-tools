# frozen_string_literal: true

module ReleaseTools
  module Metrics
    class SentryIssues
      include ::SemanticLogger::Loggable

      METRIC = :delivery_sentry_issues
      DESCRIPTION = "Number of new issues in Sentry for each environment"
      LABELS = %i[role].freeze

      SENTRY_ENDPOINT = 'https://sentry.gitlab.net/api/0/projects/gitlab/gitlabcom/issues/'

      def initialize
        @prometheus = ReleaseTools::Prometheus::Query.new

        @registry = ::Prometheus::Client::Registry.new
        @registry.unregister(METRIC)

        @push = Metrics::Push.new(METRIC)
        @metric = @registry.gauge(METRIC, docstring: DESCRIPTION, labels: LABELS)
      end

      def execute
        # Index counts by gitlab-rails SHA so multiple environments running
        # the same version only perform one API call
        counts = {}

        @prometheus.rails_version_by_role.each do |role, sha|
          next if sha.blank?

          sha = sha[0...11]
          counts[sha] ||= sentry_issues(sha)

          logger.info('Sentry issues', release: sha, issues: counts[sha])

          @metric.set(counts[sha], labels: LABELS.zip([role]).to_h)
        end

        @push.replace(@registry)
      end

      private

      def sentry
        @sentry ||= HTTP.auth("Bearer #{ENV.fetch('SENTRY_AUTH_TOKEN', nil)}")
      end

      def sentry_issues(sha)
        # NOTE: Staging has a separate `staginggitlabcom` project, but staging
        # sees no real-world usage, so its project isn't a great representation
        # of a release's health
        response = sentry.get(
          SENTRY_ENDPOINT,
          params: { query: "first-release:#{sha} is:unresolved" }
        )

        if response.status.ok?
          response['X-Hits'].to_i
        else
          logger.warn(
            "Failed to fetch Sentry issues",
            release: sha,
            error_code: response.code,
            error_message: response.reason
          )

          0
        end
      end
    end
  end
end

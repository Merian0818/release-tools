# frozen_string_literal: true

module ReleaseTools
  module Services
    # Makes sure a merge request is automatically merged
    class AutoMergeService
      include ::SemanticLogger::Loggable

      attr_reader :merge_request, :commit

      # Initialize the service
      #
      # @param merge_request [ReleaseTools::MergeRequest] the merge request to approve
      # @param token [string] the GitLab private token, it should not belong to the merge request's author
      # @param commit [Gitlab::ObjectifiedHash] an optional commit object
      def initialize(merge_request, token:, commit: nil)
        @merge_request = merge_request
        @token = token
        @commit = commit
      end

      def execute
        if GitlabClient.merge_trains_enabled?(merge_request.project)
          add_to_merge_train
        else
          merge_when_pipeline_succeeds
        end
      end

      private

      attr_reader :token

      def add_to_merge_train
        logger
          .info('Adding merge request to Merge train', merge_request: merge_request.url)

        Services::AddToMergeTrainService
          .new(merge_request, token: token)
          .execute(commit.id)
      end

      def merge_when_pipeline_succeeds
        logger
          .info('Accepting merge request', merge_request: merge_request.url)

        Services::MergeWhenPipelineSucceedsService
          .new(merge_request, token: token)
          .execute
      end
    end
  end
end

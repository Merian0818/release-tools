# frozen_string_literal: true

module ReleaseTools
  module Rollback
    # Generates a `Rollback::Comparison` between a current state and a target to
    # which we want to roll back.
    #
    # The `current` and the `target` values can be a package String (e.g.,
    # `14.3.202109151120-97529725116.5a421e7fb6d`), or an environment name
    # (e.g., `gprd`).
    #
    # If given an environment name, we fetch the Deployments for that
    # environment. When an environment is used as the `current` argument, we
    # grab the latest Deployment regardless of status; when used as the `target`
    # argument, we grab the latest successful Deployment that isn't the
    # current one.
    #
    # Examples:
    #
    #   # Comparing two specific packages
    #   CompareService.new(
    #     current: '14.3.202109151620-603b96d4843.7b4f35cc217',
    #     target: '14.3.202109151120-97529725116.5a421e7fb6d'
    #   ).execute
    #
    #   # Comparing current and previous `gprd` deployments
    #   CompareService.new(current: 'gprd', target: 'gprd').execute
    #
    #   # Comparing current `gprd-cny` deployment to current `gprd` deployment
    #   CompareService.new(current: 'gprd-cny', target: 'gprd').execute
    #
    #   # Comparing current `gprd` deployment to a specific version
    #   CompareService
    #     .new(current: 'gprd', target: '14.3.202109151120-97529725116.5a421e7fb6d')
    #     .execute
    class CompareService
      include ::SemanticLogger::Loggable

      PROJECT = Project::Release::Metadata

      ENVIRONMENTS = %w[gstg-cny gstg gprd-cny gprd].freeze

      attr_reader :current, :target

      def initialize(current:, target:)
        @current = from_package(current)
        @target = from_package(target)
      end

      def execute
        @current = current_from_deployment(@current) if environment?(@current)
        @target = target_from_deployment(@target) if environment?(@target)

        logger.info("Generating comparison", current: @current.to_s, target: @target.to_s)

        validate_version!(@current)
        validate_version!(@target)

        @comparison = Rollback::Comparison
          .new(current: @current, target: @target)
          .execute
      end

      private

      def validate_version!(version)
        return if version.is_a?(ProductVersion)

        logger.fatal("Invalid version for comparison", version: version.to_s)
        raise ArgumentError
      end

      def environment?(name)
        ENVIRONMENTS.include?(name.to_s)
      end

      def from_package(package)
        return package if package.nil?

        # If it doesn't match it may be an environment, which we'll load later
        return package unless AutoDeploy::Version.match?(package)

        ProductVersion.from_auto_deploy(package)
      end

      def from_metadata_commit_id(sha)
        return nil if sha.nil?

        ProductVersion.from_metadata_sha(sha)
      end

      def current_from_deployment(environment)
        from_metadata_commit_id(deployments(environment).first.sha)
      end

      def target_from_deployment(environment)
        target = deployments(environment).detect do |dep|
          dep.sha != @current.metadata_commit_id && dep.status == 'success'
        end

        from_metadata_commit_id(target&.sha)
      end

      def deployments(environment)
        Retriable.with_context(:api) do
          GitlabOpsClient.deployments(PROJECT, environment)
        end
      end
    end
  end
end

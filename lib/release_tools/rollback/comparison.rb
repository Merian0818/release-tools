# frozen_string_literal: true

module ReleaseTools
  module Rollback
    # Given a changeset comparison, determines if a rollback is possible
    class Comparison
      include ::SemanticLogger::Loggable

      PROJECT = ReleaseTools::Project::GitlabEe.auto_deploy_path

      # The currently running (or upcoming) AutoDeploy::Version
      attr_reader :current

      # The AutoDeploy::Version to which we want to rollback
      attr_reader :target

      # Array of new migrations in the diff
      attr_reader :migrations

      # Array of new post-deploy migrations in the diff
      attr_reader :post_deploy_migrations

      # Rollback cannot be performed to auto-deploy packages older than this version.
      attr_reader :minimum_version

      # current - ProductVersion which is currently running
      # target  - ProductVersion which we want to rollback to
      def initialize(current:, target:)
        @current = current
        @target = target

        @migrations = []
        @post_deploy_migrations = []
      end

      def execute
        @compare = Retriable.with_context(:api) do
          GitlabClient.compare(PROJECT, from: target_rails_sha, to: current_rails_sha)
        end

        if rollback_check_post_deploy_pipeline?
          @minimum_version = ReleaseTools::ProductVersion.from_metadata_sha(last_post_deploy_pipeline.sha)

        else
          @compare.diffs.each do |diff|
            if new_migration?(diff)
              @migrations << diff
            elsif new_post_deploy_migration?(diff)
              @post_deploy_migrations << diff
            end
          end
        end

        logger.info(
          'Rollback comparison',
          safe: safe?,
          migrations: migrations.size,
          post_deploy_migrations: post_deploy_migrations.size,
          empty: empty?,
          timeout: timeout?,
          web_url: web_url,
          rollback_check_post_deploy_pipeline: rollback_check_post_deploy_pipeline?
        )

        self
      end

      def safe?
        if rollback_check_post_deploy_pipeline?
          target >= minimum_version
        else
          post_deploy_migrations.none? && !timeout? && !empty?
        end
      end

      def timeout?
        @compare.compare_timeout
      end

      def empty?
        @compare.diffs.empty?
      end

      def web_url
        @compare.web_url
      end

      def current_package
        @current.to_s
      end

      def current_rails_sha
        @current[Project::GitlabEe.metadata_project_name].sha
      end

      def current_auto_deploy_package
        @current.auto_deploy_package
      end

      def target_package
        @target.to_s
      end

      def target_rails_sha
        @target[Project::GitlabEe.metadata_project_name].sha
      end

      def target_auto_deploy_package
        @target.auto_deploy_package
      end

      private

      def new_migration?(diff)
        diff['new_path'].start_with?('db/migrate/') && diff['new_file']
      end

      def new_post_deploy_migration?(diff)
        diff['new_path'].start_with?('db/post_migrate/') && diff['new_file']
      end

      def last_post_deploy_pipeline
        Retriable.with_context(:api) do
          # The post-deploy migration pipeline is always executed on both, gstg and gprd. So we can check `db/gprd` here even
          # if we're checking for a rollback on gstg.
          GitlabOpsClient.last_successful_deployment(ReleaseTools::Project::Release::Metadata, "db/gprd")
        end
      end

      def rollback_check_post_deploy_pipeline?
        Feature.enabled?(:rollback_check_post_deploy_pipeline)
      end
    end
  end
end

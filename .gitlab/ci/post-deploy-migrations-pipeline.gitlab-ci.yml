# Jobs associated to the post-deploy migrations pipeline https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/585

.if-post-deploy-pipeline: &if-post-deploy-pipeline
  if: '($CI_PIPELINE_SOURCE == "pipeline" || $CI_PIPELINE_SOURCE == "trigger") && $POSTDEPLOY_PIPELINE == "true"'

.post_deploy_notify_base:
  extends: .with-bundle
  variables:
    TRIGGER_REF: 'master'
    DEPLOY_USER: 'deployer'
  allow_failure: true
  script:
    - bundle exec rake 'post_deploy_migrations:notify'

.post_deploy_notify_start:
  extends: .post_deploy_notify_base
  rules:
    - <<: *if-post-deploy-pipeline
      when: on_success

.post_deploy_notify_success:
  extends: .post_deploy_notify_base
  rules:
    - <<: *if-post-deploy-pipeline
      when: on_success

.post_deploy_notify_failure:
  extends: .post_deploy_notify_base
  rules:
    - <<: *if-post-deploy-pipeline
      when: on_failure

.post_deploy_track_base:
  extends: .with-bundle
  allow_failure: true
  script:
    - bundle exec rake 'post_deploy_migrations:track_deployment'

.post_deploy_track_running:
  extends: .post_deploy_track_base
  variables:
    DEPLOY_STATUS: 'running'
  rules:
    - <<: *if-post-deploy-pipeline
      when: on_success

.post_deploy_track_success:
  extends: .post_deploy_track_base
  variables:
    DEPLOY_STATUS: 'success'
  rules:
    - <<: *if-post-deploy-pipeline
      when: on_success

.post_deploy_track_failure:
  extends: .post_deploy_track_base
  variables:
    DEPLOY_STATUS: 'failed'
  rules:
    - <<: *if-post-deploy-pipeline
      when: on_failure

# Performs default production checks allowing the post-deploy migrations
# to start, and posts a note to the monthly release issue.
post_deploy_migrations:prepare:
  extends: .with-bundle
  stage: post_deploy:prepare
  rules:
    - <<: *if-post-deploy-pipeline
  script:
    - bundle exec rake 'post_deploy_migrations:prepare'
  tags:
    # Internal Thanos pushgateway is only available from specific tagged runners
    - release

# Sends a slack notification to notify the start of the post-deploy
# migrations execution.
notify_start:post_deploy_migrations:gstg:
  extends: .post_deploy_notify_start
  stage: post_deploy:start:staging
  variables:
    DEPLOY_ENVIRONMENT: 'gstg'
  needs:
    - post_deploy_migrations:prepare

# Track a running post-deploy pipeline in the db/gstg environment
track_running:post_deploy_migrations:gstg:
  extends: .post_deploy_track_running
  stage: post_deploy:start:staging
  variables:
    DEPLOY_ENVIRONMENT: 'db/gstg'
  needs:
    - post_deploy_migrations:prepare

# Trigger a downstream pipeline to execute postdeploy migrations
# on staging
post_deploy_migrations:gstg:
  stage: post_deploy:start:staging
  rules:
    - <<: *if-post-deploy-pipeline
  variables:
    DEPLOY_ENVIRONMENT: 'gstg'
    TRIGGER_REF: 'master'
    DEPLOY_USER: 'deployer'
    POSTDEPLOY_MIGRATIONS: 'true'
    CHECKMODE: $TEST
  needs:
    - post_deploy_migrations:prepare
  trigger:
    project: gitlab-com/gl-infra/deployer
    strategy: depend

# Track a successful post-migration execution on db/gstg
track_success:post_deploy_migrations:gstg:
  extends: .post_deploy_track_success
  stage: post_deploy:track:staging
  variables:
    DEPLOY_ENVIRONMENT: 'db/gstg'
  needs:
    - track_running:post_deploy_migrations:gstg
    - post_deploy_migrations:gstg

# Track a failed post-migration execution on db/gstg
track_failed:post_deploy_migrations:gstg:
  extends: .post_deploy_track_failure
  stage: post_deploy:track:staging
  variables:
    DEPLOY_ENVIRONMENT: 'db/gstg'
  needs:
    - track_running:post_deploy_migrations:gstg
    - post_deploy_migrations:gstg

# Triggers a Full Quality pipeline on quality/staging
qa:full:post_deploy_migrations:gstg:
  stage: post_deploy:qa:staging
  rules:
    - <<: *if-post-deploy-pipeline
      when: on_success
  variables:
    DEPLOY_ENVIRONMENT: 'gstg'
    FULL_ONLY: 'true'
  needs:
    - post_deploy_migrations:gstg
  trigger:
    project: gitlab-org/quality/staging

# Triggers a Smoke Quality pipeline on quality/staging
qa:smoke:post_deploy_migrations:gstg:
  stage: post_deploy:qa:staging
  rules:
    - <<: *if-post-deploy-pipeline
      when: on_success
  variables:
    DEPLOY_ENVIRONMENT: 'gstg'
    SMOKE_ONLY: 'true'
  needs:
    - post_deploy_migrations:gstg
  trigger:
    project: gitlab-org/quality/staging
    strategy: depend

# Sends a slack notification notifying a failed QA for gstg
notify_failure:qa:post_deploy_migrations:gstg:
  extends: .post_deploy_notify_failure
  stage: post_deploy:finish:staging
  variables:
    DEPLOY_ENVIRONMENT: 'gstg'
  needs:
    - qa:smoke:post_deploy_migrations:gstg
  script:
    - bundle exec rake 'post_deploy_migrations:qa_notify'

# Sends a slack notification to notify a successful execution of post-deploy
# migrations in staging.
notify_success:post_deploy_migrations:gstg:
  extends: .post_deploy_notify_success
  stage: post_deploy:finish:staging
  variables:
    DEPLOY_ENVIRONMENT: 'gstg'
  needs:
    - notify_start:post_deploy_migrations:gstg
    - qa:smoke:post_deploy_migrations:gstg

# Sends a slack notification to notify a failed execution of post-deploy
# migrations in staging.
notify_failure:post_deploy_migrations:gstg:
  extends: .post_deploy_notify_failure
  stage: post_deploy:finish:staging
  variables:
    DEPLOY_ENVIRONMENT: 'gstg'
  needs:
    - notify_start:post_deploy_migrations:gstg
    - post_deploy_migrations:gstg

# Sends a slack notification to notify the start of the post-deploy
# migrations execution.
notify_start:post_deploy_migrations:gprd:
  extends: .post_deploy_notify_start
  stage: post_deploy:start:production
  variables:
    DEPLOY_ENVIRONMENT: 'gprd'
  needs:
    - notify_success:post_deploy_migrations:gstg

# Track a running post-deploy pipeline in the db/gprd environment
track_running:post_deploy_migrations:gprd:
  extends: .post_deploy_track_running
  stage: post_deploy:start:production
  variables:
    DEPLOY_ENVIRONMENT: 'db/gprd'
  needs:
    - notify_success:post_deploy_migrations:gstg

# Trigger a downstream pipeline to execute postdeploy migrations
# on production
post_deploy_migrations:gprd:
 stage: post_deploy:start:production
 rules:
   - <<: *if-post-deploy-pipeline
 variables:
   DEPLOY_ENVIRONMENT: 'gprd'
   TRIGGER_REF: 'master'
   DEPLOY_USER: 'deployer'
   POSTDEPLOY_MIGRATIONS: 'true'
   CHECKMODE: $TEST
 needs:
   - notify_success:post_deploy_migrations:gstg
 trigger:
   project: gitlab-com/gl-infra/deployer
   strategy: depend

# Track a successful post-migration execution on db/gprd
track_success:post_deploy_migrations:gprd:
  extends: .post_deploy_track_success
  stage: post_deploy:track:production
  variables:
    DEPLOY_ENVIRONMENT: 'db/gprd'
  needs:
    - track_running:post_deploy_migrations:gprd
    - post_deploy_migrations:gprd

# Track a failed post-migration execution on db/gprd
track_failed:post_deploy_migrations:gprd:
  extends: .post_deploy_track_failure
  stage: post_deploy:track:production
  variables:
    DEPLOY_ENVIRONMENT: 'db/gprd'
  needs:
    - track_running:post_deploy_migrations:gprd
    - post_deploy_migrations:gprd

# Sends a slack notification to notify a successful execution of post-deploy
# migrations in production.
notify_success:post_deploy_migrations:gprd:
  extends: .post_deploy_notify_success
  stage: post_deploy:finish:production
  variables:
    DEPLOY_ENVIRONMENT: 'gprd'
  needs:
    - notify_start:post_deploy_migrations:gprd
    - post_deploy_migrations:gprd

# Sends a slack notification to notify a failed execution of post-deploy
# migrations in production.
notify_failure:post_deploy_migrations:gprd:
  extends: .post_deploy_notify_failure
  stage: post_deploy:finish:production
  variables:
    DEPLOY_ENVIRONMENT: 'gprd'
  needs:
    - notify_start:post_deploy_migrations:gprd
    - post_deploy_migrations:gprd
